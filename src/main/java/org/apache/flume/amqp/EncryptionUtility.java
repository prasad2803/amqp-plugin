package org.apache.flume.amqp;

import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPrivateKey;
import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPublicKey;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.encoders.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Cipher;
import javax.crypto.KeyAgreement;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import java.security.*;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

public class EncryptionUtility {
    private static final Logger LOG = LoggerFactory.getLogger(EncryptionUtility.class);
    static Cipher aCipher = null;
    static {

        try {
            byte[] ecRemotePriKey = Base64.decode(Constants.Defaults.PRIVATE_KEY);
            PrivateKey privateKey = getPrivateKey(ecRemotePriKey);

            byte[] ecRemotePubKey = Base64.decode(Constants.Defaults.PUBLIC_KEY);
            PublicKey publicKey = getPublicKey(ecRemotePubKey);

            KeyAgreement aKeyAgree = KeyAgreement.getInstance("ECDH", "BC");
            aKeyAgree.init(privateKey);
            aKeyAgree.doPhase(publicKey, true);


            byte[] aBys = aKeyAgree.generateSecret();
            KeySpec aKeySpec = new DESKeySpec(aBys);
            SecretKeyFactory aFactory = SecretKeyFactory.getInstance("DES");
            Key aSecretKey = aFactory.generateSecret(aKeySpec);

            aCipher = Cipher.getInstance(aSecretKey.getAlgorithm());
            aCipher.init(Cipher.ENCRYPT_MODE, aSecretKey);
        } catch (Exception e) {
            LOG.error("Error occurred while initialization of encryption utility.", e);
        }
    }

    private static BCECPrivateKey getPrivateKey(byte[] ecRemotePriKey) throws Exception {
        Security.addProvider(new BouncyCastleProvider());
        KeyFactory factory = KeyFactory.getInstance("ECDSA", "BC");
        java.security.PrivateKey ecPrivateKey = (BCECPrivateKey) factory
                .generatePrivate(new PKCS8EncodedKeySpec(ecRemotePriKey)); // Helper.toByte(ecRemotePubKey)) is java.security.PublicKey#getEncoded()
        return (BCECPrivateKey) ecPrivateKey;
    }

    private static BCECPublicKey getPublicKey(byte[] ecRemotePubKey) throws Exception {
        Security.addProvider(new BouncyCastleProvider());
        KeyFactory factory = KeyFactory.getInstance("ECDSA", "BC");
        java.security.PublicKey ecPublicKey = (BCECPublicKey) factory
                .generatePublic(new X509EncodedKeySpec(ecRemotePubKey)); // Helper.toByte(ecRemotePubKey)) is java.security.PublicKey#getEncoded()
        return (BCECPublicKey) ecPublicKey;
    }

    public static String encrypt(String message) {

        if(aCipher == null) {
            LOG.error("Initialization of encryption utility has problem, so encryption wont be happen.");
            return null;
        }

        if(message == null) {
            LOG.error("Message given to encryption is invalid(null), so encryption wont be happen.");
            return null;
        }


        if(message.trim().length() == 0) {
            LOG.error("Message given to encryption is empty, so encryption wont be happen.");
            return null;
        }

        try {
            byte[] encText = aCipher.doFinal(message.getBytes());
            return new String(Base64.encode(encText));
        } catch (Exception e) {
            LOG.error("Error occurred while encrypting the message on encry      ption utility.", e);
        }
        return message;
    }
}
